<?php
	
	defined('_JEXEC') or die;
	if(!isset($params) || !(count($params) > 0)) return;
	require_once dirname(__FILE__) . '/core/helper.php';
	require_once __DIR__ . '/helper.php';
	
	$layout = $params->get('layout', 'defaultbigpig2');
	$cacheid = md5(serialize(array($layout, $module->id)));
	$cacheparams = new stdClass;
	$cacheparams->cachemode = 'id';
	$cacheparams->class = 'BigPigGoodIdeasHelper';
	$cacheparams->method = 'getList';
	$cacheparams->methodparams = $params;
	$cacheparams->modeparams = $cacheid;
	$list = JModuleHelper::moduleCache($module, $params, $cacheparams);
	$session = JFactory::getSession();
	$session->set('mod_bigpig_good_ideas', $list );
	
	$defaultbigpig = array_slice($list,0,1);
	
	require JModuleHelper::getLayoutPath('mod_bigpig_good_ideas', 'defaultbigpig2');?>

