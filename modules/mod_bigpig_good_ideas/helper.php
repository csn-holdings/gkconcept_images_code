<?php defined('_JEXEC') or die;
	use Joomla\CMS\Uri\Uri;
	class modBigPigGoodIdeasHelper
	{
		public function linkImagesAjax()
		{
			$time = $_POST['time'];
			
			if(empty($time))
			{
				return ;
			}
			
			$session = JFactory::getSession();
			$data = array_slice($session->get('mod_bigpig_good_ideas'),$time,1);
			$result = '';
			foreach ($data as $item) {
				foreach ($item->img as $ite) {
					if (is_file($ite['src']))
					{
//						$result .='<a href=" '. $item->link .'" target="_blank">';
//						$result .='<div class="masonry-item"><div class="masonry-content">';
//						$result .='<img src="'. Uri::root() . '/'.$ite['src'] .'" alt="'. $item->title .'">';
//						$result .='<h6 class="masonry-title">'. $item->title .'</h6>';
//						$result .='<p class="masonry-description">'. substr($item->introtext,0,50) . '</p></div></div> </a>';
						
						$result .='<a href=" '. $item->link .'" target="_blank">';
						$result .='<div class="masonry-item"><div class="masonry-content">';
						$result .='<img src="'. Uri::root() . '/'.$ite['src'] .'" alt="'. $item->title .'">';
						$result .='</div></div></a>';
					}
				}
			}
			
			
			return $result;
		}
	}