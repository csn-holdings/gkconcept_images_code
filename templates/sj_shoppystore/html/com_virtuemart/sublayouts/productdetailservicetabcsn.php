<!-- Nav tabs -->
<ul id="addreview" class="nav nav-tabs clearfix" role="tablist">
	<li role="presentation" class="clearfix active">
		<a href="#gioithieuchungvesanpham" aria-controls="home" role="tab"
		   data-toggle="tab"><?php echo vmText::_('Giới Thiệu Chung Về Sản Phẩm') ?></a>
	</li>
	<li role="presentation">
		<a href="#gioithieuvenhasanxuat" aria-controls="add-reviews" role="tab"
		   data-toggle="tab"><?php echo vmText::_('Giới Thiệu Về Công Ty CSN') ?></a>
	</li>
	<li role="presentation">
		<a href="#gomathong" aria-controls="messages" role="tab"
		   data-toggle="tab"><?php echo vmText::_('Gỗ Mật Hồng') ?></a>
	</li>
</ul>
<!-- Tab panes -->
<div class="tab-content clearfix">
	<div role="tabpanel" class="tab-pane clearfix active" id="gioithieuchungvesanpham">
		<div class="row">
			<div class="col-6" style="margin: 0px 45px 0px 0px;">
				<ul>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Chức năng sản phẩm : chưa có thông tin
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Sử dụng chung : chưa có thông tin </span>
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Chất liệu : chưa có thông tin </span>
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Màu sắc : chưa có thông tin </span>
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Lắp ráp sẵn : chưa có thông tin </span>
					</li>
				</ul>
			</div>
			<div class="col-6">
				<ul>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Phong cách : chưa có thông tin </span>
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Sản Xuất : chưa có thông tin </span>
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Xuất Xứ : chưa có thông tin </span>
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Gấp lại : chưa có thông tin </span>
					</li>
					<li style="font-weight: 400;"><span style="font-weight: 400;">Chống nước : chưa có thông tin </span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div role="tabpanel" class="tab-pane clearfix" id="gioithieuvenhasanxuat">
		CSN HOLDINGS xin chân thành cảm ơn Qúy khách hàng đã quan tâm đến sản phẩm do chúng tôi cung cấp.
		Công ty CSN Cam kết đồng hành và phát triển cùng quý vị.
		<br>
		Chúng tôi đảm bảo trao tận tay quý khách hàng những sản phẩm có chất lượng tốt nhất. Lấy công nghệ
		xử lý gỗ tự nhiên làm năng lực cốt lõi và thay đổi cách tiếp cận trong sản xuất CSN Holdings để sản
		xuất các sản phẩm như ván sàn, cửa gỗ, vách kính khung gỗ, nội thất gia đình, nội thất văn phòng,
		nhà hàng, cafe, các vật dụng nhà bếp, phòng tắm được làm hoàn toàn từ gỗ tự nhiên mang thương hiệu
		GKconcept - tôn tạo không gian cho cuộc sống trọn vẹn và các sản phẩm nội thất, đồ chơi, dụng cụ học
		tập cho trẻ em mang thương hiệu Jiko - cho tuổi thơ trọn vẹn được làm hoàn toàn từ gỗ tự nhiên Mật
		Hồng với chất lượng và giá thành làm hài lòng những khách hàng có gu thẩm mỹ khó tính nhất bằng
		những “NĂNG LỰC CỐT LÕI CỦA CSN”.
		<br> Cụ thể:
		<br>
		* Hệ thống Sản xuất tối ưu đi từ nguyên liệu đầu vào đến sản phẩm hoàn thiện cuối cùng, mang lại sản
		phẩm có thang giá trị-chi phí cao nhất.
		<br>
		* Tiếp cận thiết kế và sản xuất lấy nhu cầu khách hàng làm trọng tâm.
		<br>
		* Chiến Lược Trực Tiếp Và Phát Triển Thương Hiệu Tổng thể, tích hợp và đo lường.
		<br>
		* Hệ Thống Phân Phối, Bán Hàng và chăm sóc khách hàng đa dạng kênh tiếp cận nắm trực tiếp người dùng
		cuối.
		<br>
		* Tối ưu hóa trong vận hành dựa trên ứng dụng các giải pháp công nghệ.
		<br>
		Xin chân thành cảm ơn Quý khách hàng đã dành thời gian lưu tâm tới Thư ngỏ của chúng tôi. CSN rất
		hân hạnh được phục vụ Quý khách hàng trong khoảng thời gian sắp tới. Kính chúc Quý khách hàng luôn
		hạnh phúc, thành công và thịnh vượng!
		CÔNG TY TRÁCH NHIỆM HỮU HẠN SẢN XUẤT THƯƠNG MẠI VÀ DỊCH VỤ CSN HOLDINGS
		<br>
		Địa chỉ văn phòng: 57/6/16 Điện Biên Phủ, Phường 15, Quận Bình Thạnh, TP. HCM
		<br>
		Mã số thuế: 0313631211
		<br>
		Email: info@csn.com.vn
	</div>
	<div role="tabpanel" class="tab-pane clearfix" id="gomathong">
		Ưu điểm của gỗ mật hồng trong thiết kế nội thất
		Gỗ mật hồng nếu được xử lý đúng cách sẽ cho thành phẩm có khả năng chịu lực tốt, vân đẹp tự nhiên,
		bền bỉ dưới nhiều điều kiện thời tiết...
		
		<br>
		
		Nhắc đến gỗ tự nhiên, những người am hiểu và yêu thích chúng sẽ không mấy khó khăn để gọi tên một
		vài đại diện quen thuộc. Trong đó có gỗ mật hồng - loài cây gỗ phân bố chủ yếu ở bán đảo Đông Dương
		(gồm ba nước: Việt Nam, Lào, Campuchia). Khu vực này hội tụ điều kiện khí hậu và thổ nhưỡng thích
		hợp cho sự phát triển nguồn tài nguyên thực vật tự nhiên.
		
		Mật hồng là tên thương mại của thực vật có hoa thuộc họ Myrtaceace, thời gian sinh trưởng chậm, mất
		tối thiểu 20 năm mới đạt kích thước đường kính 20 cm để đúng tuổi khai thác. Loại gỗ này có xơ mịn,
		dai, chịu lực tốt, vân đẹp tự nhiên. Nếu được xử lý đúng cách, sản phẩm thấy được từng vân thớ sẽ
		tôn lên vẻ tự nhiên vốn có.
		
		Sàn nhà với gỗ mật hồng có các đường vân tự nhiên.
		
		<br>
		<img src="https://i1-giadinh.vnecdn.net/2019/12/16/205-1576467037-9346-1576479624.png?w=680&h=0&q=100&dpr=1&fit=crop&s=bw28zrCO0-4CsYW56K6F0A"
		     alt="..."/>
		<br>
		Màu hồng nhạt toát lên vẻ sang trọng và quý phái, phù hợp sử dụng trang trí nội thất hay làm các vật
		dụng trang trí, tôn tạo không gian. Các sản phẩm nội thất từ gỗ tự nhiên mật hồng có thời sử dụng
		lâu dài.
		<br>
		
		Tuy vậy gỗ có đặc tính tự nhiên là dễ cong vênh, cần có kỹ thuật công nghệ xử lý tốt để khai thác
		được đặc tính vốn có. Gỗ tự nhiên được cấu tạo cơ bản từ các xớ gỗ là sợi cellulose dài nên rất hay
		bị cong vênh, co rút và mối mọt khi đưa vào sử dụng. Xớ gỗ cũng có độ co giãn và đàn hồi cao khi
		nhiệt độ thay đổi.
		
		Để xử lý, cần người thợ thi công có tay nghề cao, am hiểu tường tận và nắm rõ bản chất thuộc tính
		của gỗ để làm ra một sản phẩm tinh tế, khắc phục những hạn chế của loại gỗ tự nhiên này. Cụ thể, gỗ
		sẽ được xẻ nhỏ, làm ngắn xớ gỗ, sau đó ghép lại theo công nghệ ép hơi kết hợp hấp nhiệt 5 giai đoạn
		cố định xớ gỗ để tạo ra tấm gỗ lớn liền khối, tránh được vấn đề cong vênh, co rút. Gỗ được biến đổi
		tính chất sinh học, loại bỏ hết chất dinh dưỡng hữu cơ nên không còn lo ngại hiện tượng mối mọt mà
		vẫn giữ nguyên nét tinh tế, độc đáo không thể thay thế của gỗ mật hồng, với xớ gỗ láng mịn, mượt đẹp
		hoàn toàn tự nhiên.
		
		<br>
		Từ cách xử lý đó, cho ra được gỗ thành phẩm gỗ mật hồng giữ được nét quý giá, vẹn nguyên đặc tính tự
		nhiên sẵn có và khắc chế được khuyết điểm ban đầu, trở thành nguyên vật liệu trong sản xuất, thiết
		kế nội thất, cho hiệu quả tốt, thời gian sử dụng dài. Thành phẩm mang tính bền vững cao, chắc chắn,
		khả năng chịu lực va đập và dẻo dai khá tốt, chịu được nhiều loại thời tiết, khí hậu của Việt Nam.
		Dù môi trường ẩm hay khô thì gỗ tự nhiên vẫn giữ được sự cứng cáp, không bị bào mòn, oxy hóa. Đặc
		biệt, với môi trường nhạy cảm như nhà tắm thì loại gỗ này vẫn không bị bong keo, ẩm mốc hay giãn nở.
		
		Gỗ mật hồng có màu sắc bền bỉ, tươi sáng, bóng láng theo thời gian sử dụng, tôn lên vân gỗ với những
		đường uốn lượn tự nhiên đẹp mắt và sang trọng. Dù tiếp xúc trực tiếp với nước thường xuyên trong
		thời gian dài, gỗ vẫn không bị xỉn màu. Sản phẩm nội thất khi đó sở hữu nhiều ưu điểm như đẹp, bền,
		chắc chắn và an toàn, thân thiện, dễ thích nghi với môi trường, dễ thích ứng với thân nhiệt độ của
		người, mang lại cảm giác ấm áp vào thời tiết lạnh hay mát dịu vào mùa nóng
	</div>
</div>
<div class="clearfix"></div>