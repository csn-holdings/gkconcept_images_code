<?php
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	$vendorModel = new VirtueMartModelVendor();
	$vendor = $vendorModel->getVendor($product->virtuemart_vendor_id);
	
	require_once JPATH_ROOT . "/components/com_congtacvien/helpers/route.php";
	$link = CTVHelperRoute::getVendorShop($product->virtuemart_vendor_id);
	$text = vmText::_('COM_VIRTUEMART_VENDOR_FORM_INFO_LBL');
	
	if(empty($vendor->vendor_store_name) )
    {
        return;
    }
?>

<div class="item">
    <div class="line">
        <div class="item-wrap ">
            <div class="item-wrap-inner">
                <div class="item-image">
                    <a href="<?php echo $link ?>" title="<?php echo $vendor->vendor_store_name ?>">
                        <img loading="lazy" src="images/stories/virtuemart/vendor/phan-mem-ban-hang-tap-hoa.png" alt="" title="">
                    </a>
                </div>
                <div class="item-info">
                    <div class="item-title">
                        <a class="item-description"  style="font-family: inherit;font-size: x-large;" href="<?php echo $link ?>" title="<?php echo $vendor->vendor_store_name ?>">
                            Cửa hàng: <?php echo $vendor->vendor_store_name ?><br>
                            <hr>
                            Số điện thoại:  <?php echo $vendor->vendor_phone ? $vendor->vendor_phone :  'chưa có' ?>
                        </a>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clr1"></div>
    </div>
</div>