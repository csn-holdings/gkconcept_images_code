<?php
	/**
	 *
	 * Show the product_buy_together
	 * @author BigPig
	 */
	// Check to ensure this file is included in Joomla!
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
?>
<div class="row">
	<h3>Chính sách mua hàng Gkconcept</h3>
</div>
<div class="row">
	
	<div class="col-3">
		<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="border: rgba(9,33,67,1) 1px solid;">
			<ul class="list_bottom">
				<li><a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Hoàn tiền</a></li>
				<li><a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Đổi trả</a></li>
				<li><a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Giao hàng</a></li>
				<li><a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Bảo Hành</a></li>
			</ul>
		</div>
	</div>
	<div class="col-9">
		<div class="tab-content" id="v-pills-tabContent">
			<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
				<h6>Hoàn tiền 300% nếu hàng giả</h6>
			</div>
			<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
				<h6>Đổi trả trong 7 ngày</h6>
			</div>
			<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
				<h6>Giao hàng  toàn quốc</h6>
			</div>
			<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
				<h6>100% Bảo Hành</h6>
			</div>
		</div>
	</div>
</div>
<style>
    .tab-content >.active >h6
    {
        display: block;
        visibility: visible;
        padding: 0px 10px;
        font-size: 15px;
        color: rgba(9, 33, 67, 1);
        border: rgba(9,33,67,1) 1px solid;
    }
    .nav
    {
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
        padding: 0px 10px;
        margin: 0px 10px 0px 0px;
    }
</style>