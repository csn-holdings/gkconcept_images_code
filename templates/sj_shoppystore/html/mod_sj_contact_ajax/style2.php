<?php
/**
 * @package Sj Contact Ajax
 * @version 1.0.1
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2013 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 *
 */
defined('_JEXEC') or die;

$tag_id = 'contact_ajax'.time().rand();
JHtml::stylesheet('templates/' . JFactory::getApplication()->getTemplate().'/html/mod_sj_contact_ajax/css/styles.css');
JHtml::stylesheet('modules/'.$module->module.'/assets/css/font-awesome.css');
if( !defined('SMART_JQUERY') && $params->get('include_jquery', 0) == "1" ){
    JHtml::script('modules/'.$module->module.'/assets/js/jquery-1.8.2.min.js');
    JHtml::script('modules/'.$module->module.'/assets/js/jquery-noconflict.js');
    define('SMART_JQUERY', 1);
}

JHtml::script('modules/'.$module->module.'/assets/js/bootstrap-tooltip.js');

ob_start();
?>

#<?php echo $tag_id ?> #map-canvas {
    height:<?php echo $params->get('map_height')?>px;
    width:<?php echo $params->get('map_width')?>px;
    max-width:100%;
};

<?php
$css = ob_get_contents();
ob_end_clean();
$document =  JFactory::getDocument();
$document->addStyleDeclaration($css);
?>

<?php if($params->get('maps_display') == 1) { ?>

<?php } ?>

<?php
    $uri=JURI::getInstance();
    $uri->setVar('contact_ajax', rand(100000,999999).time());
    $uri->setVar('ctajax_modid',$module->id);

 ?>

<!--[if lt IE 9]><div class="contact-ajax msie lt-ie9" id="<?php echo $tag_id; ?>" ><![endif]-->
<!--[if IE 9]><div class="contact-ajax msie" id="<?php echo $tag_id; ?>" ><![endif]-->
<!--[if gt IE 9]><!--><div class="contact-ajax" id="<?php echo $tag_id; ?>" ><!--<![endif]-->
    <div class="ctajax-wrap style2">
        <div class="row">
            <div class="col-sm-6">
                <div class="ctajax-element block-map">
                    <div class="el-map">
                        <div id="map-canvas"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d423283.43556854007!2d-118.69193128882294!3d34.020730494168454!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c75ddc27da13%3A0xe22fdf6f254608f4!2sLos%20Angeles%2C%20CA%2C%20USA!5e0!3m2!1sen!2s!4v1584333532609!5m2!1sen!2s" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="ctajax-element">
                    <div class="el-inner cf">
                        <div class="el-form cf">
                            <h2 class="el-heading"><span><?php echo JText::_('AJAX_CONTACT_SEND_US_A_MESSAGE');?></span></h2>
                            <form class="el-ctajax-form" id="el_ctajax_form" method="post" action="">
                                <div class="el-control">
                                    <label for="name"><?php echo JText::_('NAME_LABEL_CONTACT');?></label>
                                    <input type="text" autocomplete="off"  name="cainput_name" class="el-input" id="cainput_name" placeholder="<?php echo JText::_('NAME_LABEL_CONTACT');?>">
                                    <span class="ca-tooltip" title=""  data-toggle="tooltip" href="#" data-original-title="<?php echo JText::_('NAME_ERROR'); ?>">
                                        <i class="icon-exclamation-sign el-error"></i>
                                    </span>
                                    <i class="icon-ok-sign el-ok"></i>
                                </div>
                                <div class="el-control ">
                                    <label for="email"><?php echo JText::_('EMAIL_LABEL_CONTACT');?></label>
                                    <input autocomplete="off" type="text"  name="cainput_email" class="el-input" id="cainput_email" placeholder="<?php echo JText::_('EMAIL_LABEL_CONTACT');?>">
                                    <span class="ca-tooltip" title=""  data-toggle="tooltip" href="#" data-original-title="<?php echo JText::_('EMAIL_ERROR'); ?>">
                                        <i class="icon-exclamation-sign el-error"></i>
                                    </span>
                                    <i class="icon-ok-sign el-ok"></i>
                                </div>
                                <div class="el-control">
                                    <label for="subject"><?php echo JText::_('SUBJECT_CONTACT');?></label>
                                    <input type="text" autocomplete="off"  name="cainput_subject" class="el-input" id="cainput_subject" placeholder="<?php echo JText::_('SUBJECT_CONTACT');?>">
                                    <span class="ca-tooltip" title=""  data-toggle="tooltip" href="#" data-original-title="<?php echo JText::_('SUBJECT_ERROR'); ?>">
                                        <i class="icon-exclamation-sign el-error"></i>
                                    </span>
                                    <i class="icon-ok-sign el-ok"></i>
                                </div>
                                <div class="el-control block-message">
                                    <div class="label-message"><?php echo JText::_('MESSAGE_LABEL_CONTACT');?></div>
                                    <textarea name="cainput_message" maxlength="1000" class="el-input" id="cainput_message"></textarea>
                                    <span class="ca-tooltip" title=""  data-toggle="tooltip" href="#" data-original-title="<?php echo JText::_('MESSAGE_ERROR'); ?>">
                                        <i class="icon-exclamation-sign el-error"></i>
                                    </span>
                                    <i class="icon-ok-sign el-ok"></i>
                                </div>
                                <?php
                                if($captcha_dis == 1) {
                                    if($captcha_disable == 1 && $user->id != 0 ){
                                    }else{
                                        if($captcha_type == 1){?>
                                            <div class="el-control captcha-form">
                                                <?php  JFactory::getApplication()->triggerEvent('showCaptcha', array($module->id)); ?>
                                            </div>
                                            <div class="el-control ">
                                                <label for="subject"><?php echo JText::_('CAPTCHA_LABEL');?></label>
                                                <input type="text" name="cainput_captcha" maxlength="6" class="el-input" id="cainput_captcha" placeholder="<?php echo JText::_('CAPTCHA_LABEL');?>">
                                                <i class="icon-spinner  icon-large icon-spin el-captcha-loadding"></i>
                                                <span class="ca-tooltip" title=""  data-toggle="tooltip" href="#" data-original-title="<?php echo JText::_('CAPTCHA_ERROR'); ?>">
                                                    <i class="icon-exclamation-sign el-error"></i>
                                                </span>
                                                <i class="icon-ok-sign el-ok"></i>
                                            </div>
                                        <?php } else {  ?>
                                            <div class="el-control">
                                                <?php
                                                JPluginHelper::importPlugin('captcha');
                                                $dispatcher = JDispatcher::getInstance();
                                                $dispatcher->trigger('onInit','dynamic_recaptcha_1');
                                                ?>
                                                <div id="dynamic_recaptcha_1"></div>
                                                <span class="ca-tooltip" title=""  data-toggle="tooltip" href="#" data-original-title="<?php echo JText::_('CAPTCHA_ERROR'); ?>">
                                                    <i class="icon-exclamation-sign el-error"></i>
                                                </span>
                                                <i class="icon-ok-sign el-ok"></i>
                                            </div>
                                        <?php }
                                    }
                                }
                                ?>
                                <?php if($params->get('email_copy_dis') == 1) { ?>
                                <div class="el-control ">
                                    <input type="checkbox" value="" id="contact_email_copy" name="contact_email_copy">
                                    <label title="" class="el-label-email-copy" for="contact_email_copy" ><?php echo JText::_('SEND_MAIL_COPY'); ?></label>
                                </div>
                                <?php } ?>
                                <div class="el-control">
                                    <input type="submit"  value="<?php echo JText::_('SEND_MAIL_LABEL_CONTACT'); ?>"  id="cainput_submit">
                                    <span class="el-ctajax-loadding"></span>
                                    <span class="el-ctajax-return return-error">
                                        <i class="icon-exclamation-sign icon-large">&nbsp;&nbsp;<?php echo JText::_('MAIL_IS_NOT_SENT'); ?></i>
                                    </span>
                                    <span class="el-ctajax-return return-success">
                                        <i class="icon-ok-circle icon-large">&nbsp;&nbsp;<?php echo JText::_('MAIL_IS_SENT'); ?></i>
                                    </span>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
