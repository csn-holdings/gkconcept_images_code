<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleHikashopCategory extends Route66Rule
{
	private static $cache = array();
	protected $variables = array('option' => 'com_hikashop', 'ctrl' => 'category', 'task' => 'listing', 'cid' => '@', 'name' => '', 'id' => '', 'view' => '');

	public function getTokensValues($query)
	{
		// Cache key
		$key = (int) $query['cid'];

		// Check cache
		if (isset(self::$cache[$key]))
		{
			return self::$cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// ID
			if ($token == '{categoryId}')
			{
				$values[] = (int) $query['cid'];
				$dbQuery->select($db->qn('category_id'));
			}
			// Alias
			elseif ($token == '{categoryAlias}')
			{
				if (isset($query['name']) && $query['name'])
				{
					$values[] = $query['name'];
				}
				$dbQuery->select($db->qn('category_alias'));
			}
			// Path
			elseif ($token == '{categoryPath}')
			{
				JModelLegacy::addIncludePath(JPATH_SITE . '/plugins/route66/hikashop/models');
				$model = JModelLegacy::getInstance('Hikashop', 'Route66Model', array('ignore_request' => true));
				$category = $model->getCategory($query['cid']);
				$categoryPath = $model->getCategoryPath($category->category_id, $category->category_parent_id, array($category->category_alias));
				$dbQuery->select($db->q($categoryPath));
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			self::$cache[$key] = $values;

			return $values;
		}

		// If not let's query the database
		$dbQuery->from($db->qn('#__hikashop_category'));
		$dbQuery->where($db->qn('category_id') . ' = ' . (int) $query['cid']);
		$db->setQuery($dbQuery);
		$values = $db->loadRow();
		self::$cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'cid')
		{
			// First check that ID is not already in the URL
			if (isset($tokens['{categoryId}']))
			{
				return $tokens['{categoryId}'];
			}

			// Check for alias
			if (isset($tokens['{categoryAlias}']))
			{
				return $this->getCategoryIdFromAlias($tokens['{categoryAlias}']);
			}

			// Check for path
			if (isset($tokens['{categoryPath}']))
			{
				return $this->getCategoryIdFromPath($tokens['{categoryPath}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		if (!function_exists('hikashop_frontendLink'))
		{
			include_once JPATH_ADMINISTRATOR . '/components/com_hikashop/helpers/helper.php';
		}
		$route = hikashop_frontendLink('index.php?option=com_hikashop&ctrl=category&task=listing&cid=' . $variables['cid']);
		parse_str($route, $result);
		$Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';

		return $Itemid;
	}

	private function getCategoryIdFromAlias($alias)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('category_id'))->from($db->qn('#__hikashop_category'))->where($db->qn('category_alias') . ' = ' . $db->q($alias));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}

	private function getCategoryIdFromPath($path)
	{
		$parts = explode('/', $path);
		$alias = end($parts);

		return $this->getCategoryIdFromAlias($alias);
	}
}
