<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleHikashopProduct extends Route66Rule
{
	private static $cache = array();
	protected $variables = array('option' => 'com_hikashop', 'ctrl' => 'product', 'task' => 'show', 'cid' => '@', 'name' => '', 'id' => '', 'view' => '');

	public function getTokensValues($query)
	{
		// Detect id
		$id = isset($query['cid']) ? $query['cid'] : $query['product_id'];

		// Cache key
		$key = (int) $id;

		// Check cache
		if (isset(self::$cache[$key]))
		{
			return self::$cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// ID
			if ($token == '{productId}')
			{
				$values[] = (int) $id;
				$dbQuery->select($db->qn('product.product_id'));
			}
			// Alias
			elseif ($token == '{productAlias}')
			{
				if (isset($query['name']) && $query['name'])
				{
					$values[] = $query['name'];
				}
				$dbQuery->select($db->qn('product.product_alias'));
			}
			// Product SKU
			elseif ($token == '{productSku}')
			{
				$dbQuery->select($db->qn('product.product_code'));
			}
			// Product year
			elseif ($token == '{productYear}')
			{
				$dbQuery->select('YEAR(FROM_UNIXTIME(' . $db->qn('product.product_created') . '))');
			}
			// Product month
			elseif ($token == '{productMonth}')
			{
				$dbQuery->select('DATE_FORMAT(FROM_UNIXTIME(' . $db->qn('product.product_created') . '), "%m")');
			}
			// Product day
			elseif ($token == '{productDay}')
			{
				$dbQuery->select('DATE_FORMAT(FROM_UNIXTIME(' . $db->qn('product.product_created') . '), "%d")');
			}
			// Product date
			elseif ($token == '{productDate}')
			{
				$dbQuery->select('DATE(FROM_UNIXTIME(' . $db->qn('product.product_created') . '))');
			}
			// Category alias
			elseif ($token == '{categoryPath}')
			{
				$categoriesQuery = $db->getQuery(true);
				$categoriesQuery->select($db->qn('category_id'))->from($db->qn('#__hikashop_product_category'))->where($db->qn('product_id') . ' = ' . (int) $id)->order($db->qn('ordering'));
				$db->setQuery($categoriesQuery, 0, 1);
				$category_id = $db->loadResult();

				JModelLegacy::addIncludePath(JPATH_SITE . '/plugins/route66/hikashop/models');
				$model = JModelLegacy::getInstance('Hikashop', 'Route66Model', array('ignore_request' => true));
				$category = $model->getCategory($category_id);
				$categoryPath = $model->getCategoryPath($category_id, $category->category_parent_id, array($category->category_alias));

				$values[] = $categoryPath;
				$dbQuery->select($db->q($categoryPath));
			}
			// Category path
			elseif ($token == '{categoryAlias}')
			{
				$categoriesQuery = $db->getQuery(true);
				$categoriesQuery->select($db->qn('category_id'))->from($db->qn('#__hikashop_product_category'))->where($db->qn('product_id') . ' = ' . (int) $id)->order($db->qn('ordering'));
				$db->setQuery($categoriesQuery, 0, 1);
				$category_id = $db->loadResult();
				$categoriesQuery = $db->getQuery(true);
				$categoriesQuery->select($db->qn('category_alias'))->from($db->qn('#__hikashop_category'))->where($db->qn('category_id') . ' = ' . (int) $category_id);
				$db->setQuery($categoriesQuery);
				$categoryAlias = $db->loadResult();

				$values[] = $categoryAlias;
				$dbQuery->select($db->q($categoryAlias));
			}
			// Manufacturer alias
			elseif ($token == '{manufacturerAlias}')
			{
				$dbQuery->select($db->qn('manufacturer.category_alias'));
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			self::$cache[$key] = $values;

			return $values;
		}

		// If not let's query the database
		$dbQuery->from($db->qn('#__hikashop_product', 'product'));
		$dbQuery->leftJoin($db->qn('#__hikashop_category', 'manufacturer') . ' ON ' . $db->qn('product.product_manufacturer_id') . ' = ' . $db->qn('manufacturer.category_id'));
		$dbQuery->where($db->qn('product.product_id') . ' = ' . (int) $id);
		$db->setQuery($dbQuery);
		$values = $db->loadRow();
		self::$cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'cid')
		{

			// First check that ID is not already in the URL
			if (isset($tokens['{productId}']))
			{
				return $tokens['{productId}'];
			}

			// Check for alias
			if (isset($tokens['{productAlias}']))
			{
				return $this->getProductIdFromAlias($tokens['{productAlias}']);
			}

			// Check for SKU
			if (isset($tokens['{productSku}']))
			{
				return $this->getProductIdFromSku($tokens['{productSku}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		if (!function_exists('hikashop_frontendLink'))
		{
			include_once JPATH_ADMINISTRATOR . '/components/com_hikashop/helpers/helper.php';
		}
		$route = hikashop_frontendLink('index.php?option=com_hikashop&ctrl=product&task=show&cid=' . $variables['cid']);
		parse_str($route, $result);
		$Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';

		return $Itemid;
	}

	private function getProductIdFromAlias($alias)
	{
		if (strpos($alias, '/') !== false)
		{
			$parts = explode('/', $alias);
			$alias = end($parts);
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('product_id'))->from($db->qn('#__hikashop_product'))->where($db->qn('product_alias') . ' = ' . $db->q($alias));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}

	private function getProductIdFromSku($sku)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('product_id'))->from($db->qn('#__hikashop_product'))->where('product_code = ' . $db->q($sku));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}
}
