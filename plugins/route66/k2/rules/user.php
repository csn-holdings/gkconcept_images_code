<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleK2User extends Route66Rule
{
	private $cache = array();
	protected $variables = array('option' => 'com_k2', 'view' => 'itemlist', 'task' => 'user', 'id' => '@');

	public function getTokensValues($query)
	{
		// Cache key
		$key = (int) $query['id'];

		// Check cache
		if (isset($this->cache[$key]))
		{
			return $this->cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// ID
			if ($token == '{userId}')
			{
				$values[] = (int) $query['id'];
				$dbQuery->select($db->qn('id'));
			}
			// Name
			elseif ($token == '{userName}')
			{
				$dbQuery->select($db->qn('name'));
			}
			// Username
			elseif ($token == '{userLoginName}')
			{
				$dbQuery->select($db->qn('username'));
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			$this->cache[$key] = $values;

			return $values;
		}

		// If not let's query the database
		$dbQuery->from($db->qn('#__users'));
		$dbQuery->where($db->qn('id') . ' = ' . (int) $query['id']);
		$db->setQuery($dbQuery);
		$values = $db->loadRow();

		// Some values need processing
		$name = array_search('{userName}', $this->tokens);

		if ($name !== false)
		{
			$values[$name] = JFilterOutput::stringURLUnicodeSlug($values[$name]);
		}
		$username = array_search('{userLoginName}', $this->tokens);

		if ($username !== false)
		{
			$values[$username] = JFilterOutput::stringURLUnicodeSlug($values[$username]);
		}
		$this->cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'id')
		{
			// First check that ID is not already in the URL
			if (isset($tokens['{userId}']))
			{
				return $tokens['{userId}'];
			}

			// Check for username
			if (isset($tokens['{userLoginName}']))
			{
				return $this->getUserIdFromUsername($tokens['{userLoginName}']);
			}

			// Check for name
			if (isset($tokens['{userName}']))
			{
				return $this->getUserIdFromName($tokens['{userName}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		$route = K2HelperRoute::getUserRoute($variables['id']);
		parse_str($route, $result);
		$Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';

		return $Itemid;
	}

	private function getUserIdFromUsername($username)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id')->from('#__users')->where('username = ' . $db->q($username));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}

	private function getUserIdFromName($name)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id')->from('#__users')->where('name = ' . $db->q($name));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}
}
