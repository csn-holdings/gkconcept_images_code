<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

use Joomla\CMS\Filesystem\File;
use Joomla\CMS\Filesystem\Folder;

class Route66ModelK2 extends JModelLegacy
{
	private static $cache = array();

	public function getSitemapItems()
	{
		$user = JFactory::getUser();
		$date = JFactory::getDate();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('item.id') . ',' . $db->qn('item.title') . ',' . $db->qn('item.alias') . ',' . $db->qn('item.catid') . ',' . $db->qn('item.image_caption') . ',' . $db->qn('item.video') . ',' . $db->qn('item.video_caption') . ',' . $db->qn('item.publish_up') . ',' . $db->qn('item.language'))->from($db->qn('#__k2_items', 'item'))->where($db->qn('item.published') . ' = 1')->where($db->qn('item.trash') . ' = 0')->where($db->qn('item.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');
		$query->where('(item.publish_up = ' . $db->q($db->getNullDate()) . ' OR item.publish_up <= ' . $db->q($date->toSql()) . ')')->where('(item.publish_down = ' . $db->q($db->getNullDate()) . ' OR item.publish_down >= ' . $db->q($date->toSql()) . ')');

		if ($this->getState('categories'))
		{
			$query->where($db->qn('item.catid') . ' IN(' . implode(',', $this->getState('categories')) . ')');
		}
		$query->select($db->qn('category.name', 'categoryTitle'));
		$query->leftJoin($db->qn('#__k2_categories', 'category') . ' ON ' . $db->qn('item.catid') . ' = ' . $db->qn('category.id'));
		$query->where($db->qn('category.published') . ' = 1')->where($db->qn('category.trash') . ' = 0')->where($db->qn('category.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');

		if ($this->getState('filter.days'))
		{
			$start = JFactory::getDate(strtotime('-' . $this->getState('filter.days') . ' day', $date->toUnix()));
			$query->where($db->qn('item.publish_up') . ' >= ' . $db->q($start->toSql()));
		}
		$query->order($db->qn('item.id'));
		$db->setQuery($query, $this->getState('offset'), $this->getState('limit'));
		$items = $db->loadObjectList();

		return $items;
	}

	public function countSitemapItems()
	{
		$user = JFactory::getUser();
		$date = JFactory::getDate();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(item.id)')->from($db->qn('#__k2_items', 'item'))->where($db->qn('item.published') . ' = 1')->where($db->qn('item.trash') . ' = 0')->where($db->qn('item.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');
		$query->where('(item.publish_up = ' . $db->q($db->getNullDate()) . ' OR item.publish_up <= ' . $db->q($date->toSql()) . ')')->where('(item.publish_down = ' . $db->q($db->getNullDate()) . ' OR item.publish_down >= ' . $db->q($date->toSql()) . ')');

		if ($this->getState('categories'))
		{
			$query->where($db->qn('item.catid') . ' IN(' . implode(',', $this->getState('categories')) . ')');
		}

		if ($this->getState('filter.days'))
		{
			$start = JFactory::getDate(strtotime('-' . $this->getState('filter.days') . ' day', $date->toUnix()));
			$query->where($db->qn('item.publish_up') . ' >= ' . $db->q($start->toSql()));
		}
		$query->leftJoin($db->qn('#__k2_categories', 'category') . ' ON ' . $db->qn('item.catid') . ' = ' . $db->qn('category.id'));
		$query->where($db->qn('category.published') . ' = 1')->where($db->qn('category.trash') . ' = 0')->where($db->qn('category.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');
		$db->setQuery($query);

		return $db->loadResult();
	}

	public function getInstantArticles()
	{
		$user = JFactory::getUser();
		$date = JFactory::getDate();
		$before = JFactory::getDate($date->toUnix() - (60 * 60));
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('item') . '.*')->from($db->qn('#__k2_items', 'item'))->where($db->qn('item.published') . ' = 1')->where($db->qn('item.trash') . ' = 0')->where($db->qn('item.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');
		$query->where('(item.publish_up = ' . $db->q($db->getNullDate()) . ' OR item.publish_up <= ' . $db->q($date->toSql()) . ')')->where('(item.publish_down = ' . $db->q($db->getNullDate()) . ' OR item.publish_down >= ' . $db->q($date->toSql()) . ')');
		$query->where('(item.publish_up >= ' . $db->q($before->toSql()) . ' OR item.modified >= ' . $db->q($before->toSql()) . ')');

		if ($this->getState('categories'))
		{
			$query->where($db->qn('item.catid') . ' IN(' . implode(',', $this->getState('categories')) . ')');
		}
		$query->leftJoin($db->qn('#__k2_categories', 'category') . ' ON ' . $db->qn('item.catid') . ' = ' . $db->qn('category.id'));
		$query->where($db->qn('category.published') . ' = 1')->where($db->qn('category.trash') . ' = 0')->where($db->qn('category.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');
		$db->setQuery($query);
		$items = $db->loadObjectList();

		return $items;
	}

	public function getVideo($source)
	{
		$video = null;

		if ($source)
		{
			$video = new stdClass();
			$video->title = '';
			$video->description = '';
			$video->thumbnail = '';
			$video->location = '';
			$video->player = '';
			$video->embed = '';
			$video->extension = '';
			$extensions = array('mpg', 'mpeg', 'mp4', 'm4v', 'mov', 'wmv', 'asf', 'avi', 'ra', 'ram', 'rm', 'flv', 'swf');

			foreach ($extensions as $extension)
			{
				$needle = '{' . $extension;

				if (stripos($source, $needle) !== false)
				{
					if (stripos($source, 'remote}'))
					{
						$video->location = str_ireplace(array('{' . $extension . 'remote}', '{/' . $extension . 'remote}'), array('', ''), $source) . '.' . $extension;
					}
					else
					{
						$video->location = JUri::root(false) . 'media/k2/videos/' . str_ireplace(array('{' . $extension . '}', '{/' . $extension . '}'), array('', ''), $source) . '.' . $extension;
					}
					$video->extension = $extension;

					break;
				}
			}

			if (!$video->location)
			{
				$video->embed = JHtml::_('content.prepare', $source);
				$dom = new DOMDocument();
				$dom->loadHTML('<?xml encoding="utf-8"?>' . $video->embed);
				$iframes = $dom->getElementsByTagName('iframe');

				if ($iframes->length > 0)
				{
					$video->player = $iframes->item(0)->getAttribute('src');
				}
			}
		}

		return $video;
	}

	public function getGallery($itemId, $itemLanguage)
	{
		$gallery = array();
		$sourcePath = JPATH_SITE . '/media/k2/galleries/' . $itemId;

		if (Folder::exists($sourcePath))
		{
			$photos = Folder::files($sourcePath, 'jpg|jpeg|JPG|JPEG|gif|GIF|png|PNG|bmp|BMP');

			if (count($photos))
			{
				$language = JFactory::getLanguage();
				$defaultLanguage = $language->getDefault();

				if ($itemLanguage == '*')
				{
					$captionsFile = JPATH_SITE . '/media/k2/galleries/' . $itemId . '/' . $defaultLanguage . '.labels.txt';
				}
				else
				{
					$captionsFile = JPATH_SITE . '/media/k2/galleries/' . $itemId . '/' . $itemLanguage . '.labels.txt';
				}

				if (!File::exists($captionsFile))
				{
					$captionsFile = JPATH_SITE . '/media/k2/galleries/' . $itemId . '/labels.txt';
				}

				$captions = array();

				if (File::exists($captionsFile))
				{
					$lines = file($captionsFile);

					foreach ($lines as $line)
					{
						$parts = explode('|', $line);

						if (is_array($parts) && isset($parts[0]))
						{
							$captions[$parts[0]] = isset($parts[1]) && $parts[1] ? $parts[1] : '';
						}
					}
				}

				foreach ($photos as $photo)
				{
					$image = new stdClass();
					$image->url = JUri::root(false) . 'media/k2/galleries/' . $itemId . '/' . $photo;
					$image->title = isset($captions[$photo]) ? $captions[$photo] : '';
					$gallery[] = $image;
				}
			}
		}

		return $gallery;
	}

	public function getRelatedItems($itemId)
	{
		$items = array();
		$itemId = (int) $itemId;

		if ($itemId)
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->qn('tagID'))->from($db->qn('#__k2_tags_xref'))->where($db->qn('itemID') . ' = ' . $itemId);
			$db->setQuery($query);
			$tags = $db->loadColumn();

			if (count($tags))
			{
				$user = JFactory::getUser();
				$date = JFactory::getDate();
				$query = $db->getQuery(true);
				$query->select($db->qn('item.id') . ',' . $db->qn('item.alias') . ',' . $db->qn('item.catid'))->from($db->qn('#__k2_items', 'item'))->where($db->qn('item.published') . ' = 1')->where($db->qn('item.trash') . ' = 0')->where($db->qn('item.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');
				$query->where('(item.publish_up = ' . $db->q($db->getNullDate()) . ' OR item.publish_up <= ' . $db->q($date->toSql()) . ')')->where('(item.publish_down = ' . $db->q($db->getNullDate()) . ' OR item.publish_down >= ' . $db->q($date->toSql()) . ')');
				$query->where($db->qn('item.id') . ' != ' . $itemId);
				$query->leftJoin($db->qn('#__k2_categories', 'category') . ' ON ' . $db->qn('item.catid') . ' = ' . $db->qn('category.id'));
				$query->where($db->qn('category.published') . ' = 1')->where($db->qn('category.trash') . ' = 0')->where($db->qn('category.access') . ' IN(' . implode(',', $user->getAuthorisedViewLevels()) . ')');
				$query->leftJoin($db->qn('#__k2_tags_xref', 'xref') . ' ON ' . $db->qn('item.id') . ' = ' . $db->qn('xref.itemID'));
				$query->where($db->qn('xref.tagID') . ' IN (' . implode(',', $tags) . ')');
				$query->group($db->qn('item.id'));
				$query->order($db->qn('item.id') . ' DESC');
				$db->setQuery($query, 0, 3);
				$items = $db->loadObjectList();
			}
		}

		return $items;
	}

	public function getCategoryPath($id, $parent, $path)
	{
		$parent = (int) $parent;

		if (!$parent)
		{
			return implode('/', array_reverse($path));
		}
		$category = $this->getCategory($parent);
		$path[] = $category->alias;

		if ((int) $category->parent)
		{
			return $this->getCategoryPath($category->id, $category->parent, $path);
		}
		else
		{
			$result = implode('/', array_reverse($path));

			return $result;
		}
	}

	public function getCategory($id)
	{
		$key = (int) $id;

		if (!isset(self::$cache[$key]))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('id');
			$query->select('parent');
			$query->select('alias');
			$query->from($db->qn('#__k2_categories'));
			$query->where($db->qn('id') . ' = ' . $key);
			$db->setQuery($query);
			self::$cache[$key] = $db->loadObject();
		}

		return self::$cache[$key];
	}
}
