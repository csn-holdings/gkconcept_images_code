<?php

	defined('_JEXEC') or die('Restricted access');
	
	jimport('joomla.plugin.plugin');

	class  plgredshop_shippingmobilegift extends JPlugin
	{

		public function onAjaxUpdateCartGitNetwork()
		{
			$price = $_POST['totalFaceValue'];
			
			if (!class_exists('VmConfig')) {
				require_once(JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php');
				VmConfig::loadConfig();
			}
			if (!class_exists('VirtueMartCart')) {
				require_once(JPATH_SITE . '/components/com_virtuemart/helpers/cart.php');
			}
			
			if (!class_exists('calculationHelper'))
				require(JPATH_VM_SITE . '/helpers/cart.php');
			
			$cart = VirtueMartCart::getCart(true);
			
			$cart->cartPrices['billTotal'] = $cart->cartPrices['billTotal'] - $price ;
			
			$data = $cart->prepareAjaxDataGitnetwork();
			$cart->setCartIntoSession(false,true);
			
			
			echo json_encode($data);
		}
		
		public function onAjaxCheckCouponGitNetwork()
		{
			if(empty($_POST['coupon_code_giftnetwork']))
			{
				return;
			}
			
			$data = explode(',',$_POST['coupon_code_giftnetwork']);

			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://uat.portal.mobilegift.vn/voucher/validate",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($data),
				CURLOPT_HTTPHEADER => array(
					"Authorization: 946ba641-492c-4dac-8243-df80b5f6adcd",
					"Content-Type: application/json",
					"Merchant-Id: 98",
					"Branch-Id: 001",
					"Pos-Id: 002",
					"Pos-Transaction-No: 970235059234545345345",
					"Pos-Transaction-DateTime: 1609354920000"
				),
			));
			
			$response = curl_exec($curl);
			curl_close($curl);
			
			$data = json_decode($response);
			
			$gitnetwork = new stdClass();
			$gitnetwork->totalFaceValue = $data->totalFaceValue;
			$gitnetwork->validVoucherCodes = $data->validVoucherCodes;
			$gitnetwork->invalidVoucherCodes = $data->invalidVoucherCodes;
			$gitnetwork->times = true;
			$gitnetwork->vouchers = $data->vouchers;
			$gitnetwork->gitnetwork = $response;
			
			$session = JFactory::getSession();
			$session->set('gitnetwork', $gitnetwork );
			$session->set('orderboxme', null );
			
			echo  $response;
		}
		
		public function plgCloseGitNetwork($code)
		{
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://uat.portal.mobilegift.vn/voucher/redeem",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($code->validVoucherCodes),
				CURLOPT_HTTPHEADER => array(
					"Authorization: 946ba641-492c-4dac-8243-df80b5f6adcd",
					"Content-Type: application/json",
					"Merchant-Id: 98",
					"Branch-Id: 001",
					"Pos-Id: 002",
					"Pos-Transaction-No: 970235059234545345345",
					"Pos-Transaction-DateTime: 1609354920000"
				),
			));
			
			$response = curl_exec($curl);
			curl_close($curl);
			
			$session = JFactory::getSession();
			$session->set('gitnetwork', null );
//			$session->set('orderboxme', null );
			
			echo  $response;
		}
		
		
	}

?>