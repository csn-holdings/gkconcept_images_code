<?php

/**
 * @package         Google Structured Data
 * @version         4.7.0 Free
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2019 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

namespace GSD;

defined('_JEXEC') or die('Restricted access');

/**
 *  Google Structured Data Product Plugin Base
 */
class PluginBaseArticle extends \GSD\PluginBase
{
	
}

?>