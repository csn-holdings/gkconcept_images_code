<?php

function ir_set_image_names(&$obj, &$dbx,$pr_id){
	global $SETTINGS;
	$iquery="SELECT file_url, file_url_thumb,  file_title, file_description, file_meta FROM #__virtuemart_product_medias AS pm JOIN #__virtuemart_medias AS vm ON vm.virtuemart_media_id=pm.virtuemart_media_id WHERE virtuemart_product_id=".$pr_id." ORDER BY vm.virtuemart_media_id ASC";
	$dbx->setQuery($iquery);
	$images = $dbx->loadObjectList();
	
	for($IN = 0 ; $IN < $SETTINGS->image_renamer->images; $IN++){
		if($IN < count($images)){
			$obj->{"image" . ($IN + 1) ."name"} = basename($images[$IN]->file_url);	
			if($SETTINGS->image_renamer->thumbs_names)
				$obj->{"thumb" . ($IN + 1) ."name"} = basename($images[$IN]->file_url_thumb);	
		}else{
			
			$obj->{"image" . ($IN + 1) ."name"} = "";
			if($SETTINGS->image_renamer->thumbs_names)
				$obj->{"thumb" . ($IN + 1) ."name"} = "";	
		}
	}
}

function ir_fileRealyExists($path){
	return file_exists($path) && !is_dir($path);
}


function ir_createMedia(&$dbx, $user_id, $image_name,$thumb_name){
	global $media_product_path;
	$ext = explode(",",$image_name);
	$ext = $ext[count($ext) - 1];
	
	$q = "INSERT INTO #__virtuemart_medias( virtuemart_media_id,virtuemart_vendor_id,file_title,file_description,file_meta,file_mimetype,file_type,file_url,file_url_thumb,file_lang ,file_is_product_image,file_is_downloadable,file_is_forSale,file_params,shared,published,created_on,created_by,modified_on,modified_by,locked_on,locked_by) VALUES (
								   NULL
								  ,0
								  ,". $dbx->quote($image_name) ."
								  ,''
								  ,''
								  ,'image/$ext'
								  ,'product'
								  ," . $dbx->quote("$media_product_path/" .$image_name) . "
								  ," . $dbx->quote("$media_product_path/resized/" .$thumb_name) . "
								  ,''
								  ,1
								  ,0
								  ,0
								  ,''
								  ,0
								  ,1
								  ,NOW()
								  ,$user_id
								  ,NOW()
								  ,$user_id
								  ,'0000-00-00 00:00:00'
								  ,0
								)";
	
	$dbx->setQuery($q);
	$dbx->query();	
	return $dbx->insertid();
}

function ir_createMediaDuplicate(&$dbx, $virtuemart_media_id){
	global $vmedia_columns;
	if(!isset($vmedia_columns)){
		$dbx->setQuery("SHOW COLUMNS FROM #__virtuemart_medias;");
		$cols = $dbx->loadObjectList();
		$vmedia_columns = array();
		foreach($cols as $col){
			if("virtuemart_media_id" == $col->Field)
				continue;
			
			$vmedia_columns[] = $col->Field; 	
		}
	}
	
	$dbx->setQuery(" INSERT INTO #__virtuemart_medias (". implode(",",$vmedia_columns) .")
					SELECT ". implode(",",$vmedia_columns) ." FROM #__virtuemart_medias
					WHERE 
					 virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
	$dbx->query();	
	return $dbx->insertid();
}

function ir_changeProductMedia(&$dbx,$virtuemart_product_id, $old_virtuemart_media_id, $virtuemart_media_id, $order = 0){
	
	if($old_virtuemart_media_id){
		$dbx->setQuery("DELETE FROM #__virtuemart_product_medias WHERE virtuemart_product_id = $virtuemart_product_id AND virtuemart_media_id = $old_virtuemart_media_id;");
		$dbx->query();
	}
	
	$i_sql = "INSERT INTO #__virtuemart_product_medias(id,virtuemart_product_id,virtuemart_media_id ,ordering) VALUES (
									   NULL
									  ,". $virtuemart_product_id ."
									  ,". $virtuemart_media_id ."
									  ,". $order ."
									)";
						
	$dbx->setQuery($i_sql);
	$dbx->query();
	
	return $dbx->insertid();
}




function ir_updateMediaByCondition(&$dbx, $condition, $image_name = NULL, $thumb_name = NULL){
	if($image_name ||  $thumb_name){
		global $media_product_path;
		$upd = array();
		
		if($image_name !== NULL){
			$upd[] = "file_url = " . $dbx->quote("$media_product_path/" .$image_name) . " ";	
		}
		
		if($thumb_name !== NULL){
			$upd[] = "file_url_thumb = " . $dbx->quote("$media_product_path/resized/" .$thumb_name) . " ";	
		}
		
		$dbx->setQuery("UPDATE #__virtuemart_medias
						SET
						 " . implode(" , ",$upd). "
					   WHERE 
						 " . $condition . " ;");
						 
		return $dbx->query();
	}
	
	return false;
}

function ir_updateMedia(&$dbx, $virtuemart_media_id, $image_name = NULL, $thumb_name = NULL){
	return ir_updateMediaByCondition($dbx," virtuemart_media_id  = $virtuemart_media_id ",$image_name,$thumb_name);
}

function ir_getMediaCount(&$dbx, $virtuemart_media_id){
	$dbx->setQuery("SELECT count(virtuemart_media_id) FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $virtuemart_media_id);
	return $dbx->loadResult();
}

function ir_loadProductImages(&$dbx,$virtuemart_product_id){
	$iquery="SELECT file_url, file_url_thumb, file_title, file_description, file_meta, pm.virtuemart_media_id FROM #__virtuemart_product_medias AS pm JOIN #__virtuemart_medias AS vm ON vm.virtuemart_media_id=pm.virtuemart_media_id WHERE virtuemart_product_id=". $virtuemart_product_id ." ORDER BY vm.virtuemart_media_id ASC";
	$dbx->setQuery($iquery);
	return $dbx->loadObjectList();
}

if(!isset($SETTINGS->image_renamer)){
	$SETTINGS->image_renamer = new stdClass;
	$SETTINGS->image_renamer->images = 0;
}

if(!isset($SETTINGS->image_renamer->use_in_export))
	$SETTINGS->image_renamer->use_in_export = 0;

if(!isset($SETTINGS->image_renamer->use_in_import))
	$SETTINGS->image_renamer->use_in_import = 0;	

if(!isset($SETTINGS->image_renamer->thumbs_names))
	$SETTINGS->image_renamer->thumbs_names = 0;	

if(!isset($SETTINGS->image_renamer->overwrite))
	$SETTINGS->image_renamer->overwrite = 0;	

if(!isset($SETTINGS->image_renamer_api_key))
	$SETTINGS->image_renamer_api_key = "";


if(isset($_REQUEST['image_renamer_save'])){
	if($_REQUEST['image_renamer_save']){
		$json = file_get_contents('php://input');
		$SETTINGS->image_renamer = json_decode($json);
		SaveSettings($db, $SETTINGS);
		echo '{"saved":"ok"}';
		die;
		return;
	}
}

if(isset($_REQUEST['image_renamer_save_api_key'])){
	if($_REQUEST['image_renamer_save_api_key']){
		
		$SETTINGS->image_renamer_api_key = $_REQUEST['image_renamer_save_api_key'];
		SaveSettings($db, $SETTINGS);
		echo '{"saved":"ok"}';
		die;
		return;
	}
}



if(!$SETTINGS->image_renamer_api_key) $SETTINGS->image_renamer->images = 0;
?>