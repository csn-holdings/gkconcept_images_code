<?php
if(isset($SETTINGS->image_renamer)){
	if($SETTINGS->image_renamer){
		global $media_product_path;
		$products_images = null;
		
		$products_images = ir_loadProductImages($db,$key);
		if(!$products_images)
			$products_images = array();
		
		$ir_output = false;
		$dependant = array();
		
		for($IN = 0 ; $IN < $SETTINGS->image_renamer->images; $IN++){
			$name = "";
			
			if(isset($task->{"image".($IN + 1)."name"})){
				$ir_output = true;
				if(!$task->{"image".($IN + 1)."name"}){
					$db->setQuery("DELETE FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
					$db->query();
					
					$db->setQuery("SELECT count(virtuemart_media_id) FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
					
					$count = $db->loadResult();
					if(!$count)
						$count = 0;
					
					if($count < 2){
						$db->setQuery("DELETE FROM #__virtuemart_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
						$db->query();		
					}
					
				} else {
					$newname = $task->{"image".($IN + 1)."name"};
					
					$images_path = JPATH_SITE. DIRECTORY_SEPARATOR . $media_product_path . DIRECTORY_SEPARATOR;
					
					if( count($products_images) > $IN){
						if(strcasecmp( basename($products_images[$IN]->file_url) , $newname) !== 0){
							
							
							$oldname = basename($products_images[$IN]->file_url);
							if(is_numeric($oldname))
								$oldname = "";
							
							$old_path = $images_path . $oldname;
							$new_path = $images_path . $newname;
							
							if($newname && strcasecmp($oldname,$newname) !== 0){
								
								
								if(!ir_fileRealyExists($old_path)){
									
									$name = "";
									if(ir_fileRealyExists($new_path)){
										
										$name       = $newname;
										
										
										if($SETTINGS->image_renamer->thumbs_names){
											$db->setQuery("UPDATE #__virtuemart_medias
															SET
															 file_url        = '$media_product_path/$newname'
														   WHERE 
															 file_url       LIKE '$media_product_path/$oldname' ");
										}else{
											$new_t_name = $newname;
											if(!ir_fileRealyExists($images_path . 'resized'. DS .$new_t_name)){
												$new_t_name = $products_images[$IN]->file_url_thumb;
											}
											
											$db->setQuery("UPDATE #__virtuemart_medias
															SET
															 file_url        = '$media_product_path/$newname',
															 file_url_thumb  = '$media_product_path/resized/$new_t_name'
														   WHERE 
															 file_url       LIKE '$media_product_path/$oldname' ");
										}
										
										$db->query();
										
										
									}else{
										
										$db->setQuery("DELETE FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
										$db->query();
										
										$db->setQuery("SELECT count(virtuemart_media_id) FROM #__virtuemart_product_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
						
										$count = $db->loadResult();
										if(!$count)
											$count = 0;
										
										if($count < 2){
											$db->setQuery("DELETE FROM #__virtuemart_medias WHERE virtuemart_media_id = " . $products_images[$IN]->virtuemart_media_id);
											$db->query();		
										}
									}
									
									
								}else{
									
									$ok       = false;
									$existed  = false;
									
									if($SETTINGS->image_renamer->overwrite){
										$ok = rename($old_path,$new_path); 
									}else{
										if(ir_fileRealyExists($new_path)){
											$ok      = true;
											$existed = true;
										}else
											$ok = rename($old_path,$new_path); 
									}
									
									if($ok){
										$name       = $newname;
										$new_t_name = "";
										
										if(!$SETTINGS->image_renamer->thumbs_names){
											$old_t_name = basename($products_images[$IN]->file_url_thumb);
											$new_t_name = $newname;
											if(ir_fileRealyExists($images_path . 'resized'. DS .$old_t_name)){
												$ok      = false;
												if($SETTINGS->image_renamer->overwrite){
													$ok = rename($images_path . 'resized'. DS .$old_t_name, $images_path . 'resized'. DS .$new_t_name);
												}else{
													if(ir_fileRealyExists($images_path . 'resized'. DS .$new_t_name)){
														$ok = true;
													}else
														$ok = rename($images_path . 'resized'. DS .$old_t_name, $images_path . 'resized'. DS .$new_t_name);
												}
												if(!$ok){
													$new_t_name = $old_t_name;
												}
											}
										}
										
										
										$mds = null;
										if($existed){
											$mds = array($products_images[$IN]->virtuemart_media_id);
											if($SETTINGS->image_renamer->thumbs_names){
												ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, $name, NULL);
											}else{
												ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, $name, $new_t_name);
											}
										}else{
										
											$db->setQuery("SELECT virtuemart_media_id FROM #__virtuemart_medias WHERE file_url LIKE '$media_product_path/$oldname' ");
											$mds = $db->loadColumn();
											
											if($SETTINGS->image_renamer->thumbs_names){
												ir_updateMediaByCondition($db, "file_url LIKE " . $db->quote("$media_product_path/" . $oldname) . ' ' , $name, NULL);
											}else{
												ir_updateMediaByCondition($db, "file_url LIKE " . $db->quote("$media_product_path/" . $oldname) . ' ' , $name, $new_t_name);
											}
										}
										
										if(!empty($mds)){
											$db->setQuery("SELECT DISTINCT virtuemart_product_id FROM #__virtuemart_product_medias WHERE virtuemart_media_id IN (" . implode(",",$mds) . ")" );
											$pds = $db->loadColumn();
											
											foreach($pds as $pd){
												$dependant[] = $pd;
											}
										}
										
									}else{
										$name = $oldname;
									}
								}
							}else{
								$name = "";
							}
							
						}else
							$name = $newname;
					}else{
						//create new
						if($newname){
							$new_path = $images_path . $newname;
							if(ir_fileRealyExists($new_path)){
								
							   $name = $newname;
							   
							   $thumb_path = $images_path . 'resized'. DS . $newname;
							   $thumb_name = "";
							   
							   if(ir_fileRealyExists($thumb_path))
								   $thumb_name = $newname;
							   
							   $mid = ir_createMedia($db, $user->id, $newname ,$thumb_name);	
							   if($mid){
									ir_changeProductMedia($db,$key, NULL , $mid, $IN + 1);
									
									$products_images = ir_loadProductImages($db,$key);
									
									
							   }
							}
						}
					}
				}
			}
			
			if($SETTINGS->image_renamer->thumbs_names){
				if(isset($task->{"thumb".($IN + 1)."name"})){
					$ir_output = true;
					if(!$task->{"thumb".($IN + 1)."name"}){
						
						
						$oldname = basename($products_images[$IN]->file_url_thumb);
						$thumbs_path = JPATH_SITE. DS . 'images' . DS . 'stories' . DS . 'virtuemart' . DS . 'product'. DS .'resized' . DS;
						$old_path = $thumbs_path . $oldname;
								
						if(is_numeric($oldname))
							$oldname = "";
							
						if($oldname){
							//if(ir_fileRealyExists($old_path)){
								//
							//}
						}
						
						$count = ir_getMediaCount($db, $products_images[$IN]->virtuemart_media_id);
						if($count > 1){
							$mid = ir_createMediaDuplicate($db, $products_images[$IN]->virtuemart_media_id);
							if($mid){
								if(ir_changeProductMedia($db, $key ,$products_images[$IN]->virtuemart_media_id ,$mid,$IN + 1)){
									$products_images[$IN]->virtuemart_media_id = $mid;
								}
								ir_updateMedia($db, $mid, NULL, '');
								$products_images = ir_loadProductImages($db,$key);
							}
						}else if($count == 1){
							ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, NULL, '');
						}
						
					} else {
						if( count($products_images) > $IN){
							$oldname = basename($products_images[$IN]->file_url_thumb);
							if(is_numeric($oldname))
								$oldname = "";
							
							if(strcasecmp( $oldname , $newname) !== 0){
								$newname = $task->{"thumb".($IN + 1)."name"};
								
								$thumbs_path = JPATH_SITE. DS . 'images' . DS . 'stories' . DS . 'virtuemart' . DS . 'product'. DS .'resized' . DS;
								$new_path = $thumbs_path . $newname;
								$old_path = $thumbs_path . $oldname;
								
								$name = "";
								
								if($oldname && $newname){
									
									if(ir_fileRealyExists($old_path) && !ir_fileRealyExists($new_path)){
										if(rename($old_path,$new_path)){
											$name = $newname;
										}
									}else if(ir_fileRealyExists($old_path) && ir_fileRealyExists($new_path)){
										if($SETTINGS->image_renamer->overwrite){
											unlink($new_path);
											if(rename($old_path,$new_path)){
												$name = $newname;
											}
										}else{
											$name = $newname;
										}
									}else if(!ir_fileRealyExists($old_path) && ir_fileRealyExists($new_path)){
										$name = $newname;
									}
								}else if($newname){
									if(ir_fileRealyExists($new_path)){
										$name = $newname;
									}
								}else{
									//
								}
								
								if($name){
									$count = ir_getMediaCount($db, $products_images[$IN]->virtuemart_media_id);
									if($count > 1){
										$mid = ir_createMediaDuplicate($db, $products_images[$IN]->virtuemart_media_id);
										if($mid){
											if(ir_changeProductMedia($db, $key ,$products_images[$IN]->virtuemart_media_id , $mid ,$IN + 1)){
												$products_images[$IN]->virtuemart_media_id = $mid;
											}
											ir_updateMedia($db, $mid, NULL, $name);
											$products_images = ir_loadProductImages($db,$key);
										}
									}else if($count == 1){
										ir_updateMedia($db, $products_images[$IN]->virtuemart_media_id, NULL, $name );
									}
								}
							}
						}
					}
				}
			}
		}
		
		if($ir_output){
			if(!$res_item->returned)
				$res_item->returned = new stdClass;
				
			ir_set_image_names($res_item->returned, $db, $key);
			
			if(!empty($dependant)){
				foreach($dependant as $dp){
					if(!isset($res_item->dependent))
						$res_item->dependent = array();
					if(!isset($res_item->dependent[$pd]))
						$res_item->dependent[$pd] = new stdClass;
					
					ir_set_image_names($res_item->dependent[$pd], $db, $pd);
				}
			}			
		}
	}
}	

?>

