<?php
/**
 * @package Helix Ultimate Framework
 * @author JoomShaper https://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2018 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
 */

defined ('_JEXEC') or die();

if (!class_exists( 'VmConfig' )) require(JPATH_ROOT .'/administrator/components/com_virtuemart/helpers/config.php');

VmConfig::loadConfig();


class VmShopperHelper {

    protected $vendor;
    /**
     * @var JInput
     */
    private $input;
    /**
     * @var string
     */
    private $sessionId;
    /**
     * @var \Joomla\CMS\Application\CMSApplication
     */
    private $app;
    /**
     * @var \Joomla\Registry\Registry
     */
    private $config;
    /**
     * @var \Joomla\CMS\User\User
     */
    private $user;

    public function __construct()
    {
        $this->input = JFactory::getApplication()->input;
        $session = JFactory::getSession();
        $this->sessionId = $session->getId();
        // Get configuration
        $this->app    = JFactory::getApplication();
        $this->config = JFactory::getConfig();
        $this->user = JFactory::getUser();

    }


    public function getVendor() {

        $option = $this->input->getWord('option');
        $view = $this->input->getWord('view');
        $vendor_id = $this->input->getInt('id');

        $vendor = null;

        if ($option == 'com_congtacvien' && $view =='shop' && $vendor_id > 0) {

            $model = new VirtueMartModelVendor();
            $vendor = $model->getVendor($vendor_id);
            $model->addImages($vendor);

        }

        return $vendor;
    }

}